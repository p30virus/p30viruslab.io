---
title: Uso del conector de script en modo iterador
subtitle: Iterar objetos de forma nativa en SDI
date: 2019-09-12
comments: true
tags: ["SDI", "WebService", "TDI", "Security Directory Integrator", "Script Connector"]
---

En el pasado, me encontre revisando algunos conectores de ISIM desarrollados hace mucho tiempo en la plataforma, durante estas actividades validando las lineas ejecutadas en SDI encontre que las llamadas a los servicios web usaban los conectores de axis por medio de script y que se encarban de realizar todo el procesamiento de los ciclos sin aprovechar la funcionalidad nativa de SDI.

Despues de ver esto tuve la curiosidad de implementar esta logica de forma nativa en un conector de script y este es el resultado.

Este ejemplo lo dividiremos en tres partes:

1. Uso del conector de funcion Axix2 FC.
2. Creacion del conector de script personalizado.

---

Para el conector [Axis2 FC]( https://www.ibm.com/support/knowledgecenter/en/SSCQGF_7.2.0/com.ibm.IBMDI.doc_7.2/referenceguide23.htm#axis2wsserverconn ) hay que dejar esto claro, no es necesario utilizar los conectores anteriores ni tipos complejos, ya que nosotros mismos nos encargaremos de especificar los nodos utilizando las herramientas nativas de SDI, el resultado final sera algo similar a archivo JSON.

Lo primero que realizaremos es definir el servicio web que usaremos [delayedstockquote]( http://ws.cdyne.com/delayedstockquote/delayedstockquote.asmx), este nos permitirá experimentar con un array de respuesta para obtener múltiples valores de respuesta y poder iterar sobre ellos.

Para poder construir el llamado es necesario que tener claro el esquema del WebService el siguiente es un ejemplo de un llamado al WebService

```
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="http://ws.cdyne.com/">
   <soapenv:Header/>
   <soapenv:Body>
      <ws:GetQuoteDataSet>
         <ws:StockSymbols>,</ws:StockSymbols>
         <ws:LicenseKey>0</ws:LicenseKey>
      </ws:GetQuoteDataSet>
   </soapenv:Body>
</soapenv:Envelope>
```

En este caso ya sabemos cómo debe ser construido el request y para eso utilizamos este código.

```
var StockSymbols =  work.createElementNS("http://ws.cdyne.com/", "StockSymbols")
var LicenseKey =  work.createElementNS("http://ws.cdyne.com/", "LicenseKey")
var GetQuoteDataSet =  work.createElementNS("http://ws.cdyne.com/", "GetQuoteDataSet")
StockSymbols.appendChild(work.createTextNode(","));
LicenseKey.appendChild(work.createTextNode("0"));
GetQuoteDataSet.appendChild(StockSymbols);
GetQuoteDataSet.appendChild(LicenseKey);
work.setAttribute("GetQuoteDataSet",GetQuoteDataSet)
task.logmsg("ScriptConn: ",work);
```
Al ejecutar el proceso debemos obtener la siguiente estructura:

```
12:18:13,372 ScriptConn:  - {
	"GetQuoteDataSet": {
		"StockSymbols": ",",
		"LicenseKey": "0"
	}
}
```
Esto nos permite emular la construcción de los nodos requeridos por el WebService.

Ahora, para este caso queremos poblar los parámetros de conexión del conector de forma dinámica para esto en el "hook" "Antes de inicializar" utilizaremos este código.

```
thisComponent["function"].setParam("wsdlUrl",WebServiceWSDL);
thisComponent["function"].setParam("service",WebServiceService);
thisComponent["function"].setParam("operation",WebServiceOperation);
thisComponent["function"].setParam("endpoint",WebServiceEndpoint);

```

Al ejecutar el proceso obtenemos la siguiente respuesta del WebService

```
12:30:25,035 INFO  - CTGDIS003I *** Start dumping Entry
12:30:25,037 INFO  - {
12:30:25,038 INFO  - 	"GetQuoteDataSet": {
12:30:25,040 INFO  - 		"StockSymbols": ",",
12:30:25,040 INFO  - 		"LicenseKey": "0"
12:30:25,041 INFO  - 	},
12:30:25,042 INFO  - 	"GetQuoteDataSetResponse": {
12:30:25,043 INFO  - 		"@xmlns": "http://ws.cdyne.com/",
12:30:25,044 INFO  - 		"GetQuoteDataSetResult": {
12:30:25,045 INFO  - 			"xs:schema": {
12:30:25,046 INFO  - 				"@xmlns": "",
12:30:25,046 INFO  - 				"@xmlns:xs": "http://www.w3.org/2001/XMLSchema",
12:30:25,048 INFO  - 				"@xmlns:msdata": "urn:schemas-microsoft-com:xml-msdata",
12:30:25,050 INFO  - 				"@id": "QuoteData",
12:30:25,051 INFO  - 				"xs:element": {
12:30:25,052 INFO  - 					"@name": "QuoteData",
12:30:25,052 INFO  - 					"@msdata:IsDataSet": "true",
12:30:25,053 INFO  - 					"@msdata:UseCurrentLocale": "true",
12:30:25,053 INFO  - 					"xs:complexType": {
12:30:25,054 INFO  - 						"xs:choice": {
12:30:25,054 INFO  - 							"@minOccurs": "0",
12:30:25,055 INFO  - 							"@maxOccurs": "unbounded",
12:30:25,056 INFO  - 							"xs:element": {
12:30:25,056 INFO  - 								"@name": "Quotes",
12:30:25,057 INFO  - 								"xs:complexType": {
12:30:25,058 INFO  - 									"xs:sequence": {
12:30:25,059 INFO  - 										"xs:element": {
12:30:25,059 INFO  - 											"@name": "StockSymbol",
12:30:25,060 INFO  - 											"@type": "xs:string",
12:30:25,061 INFO  - 											"@minOccurs": "0"
12:30:25,061 INFO  - 										},
12:30:25,062 INFO  - 										"xs:element": {
12:30:25,062 INFO  - 											"@name": "LastTradeAmount",
12:30:25,063 INFO  - 											"@type": "xs:decimal",
12:30:25,064 INFO  - 											"@minOccurs": "0"
12:30:25,065 INFO  - 										},
12:30:25,067 INFO  - 										"xs:element": {
12:30:25,067 INFO  - 											"@name": "LastTradeDateTime",
12:30:25,067 INFO  - 											"@type": "xs:dateTime",
12:30:25,068 INFO  - 											"@minOccurs": "0"
12:30:25,069 INFO  - 										},
12:30:25,070 INFO  - 										"xs:element": {
12:30:25,070 INFO  - 											"@name": "StockChange",
12:30:25,070 INFO  - 											"@type": "xs:decimal",
12:30:25,071 INFO  - 											"@minOccurs": "0"
12:30:25,072 INFO  - 										},
12:30:25,072 INFO  - 										"xs:element": {
12:30:25,072 INFO  - 											"@name": "OpenAmount",
12:30:25,072 INFO  - 											"@type": "xs:decimal",
12:30:25,073 INFO  - 											"@minOccurs": "0"
12:30:25,073 INFO  - 										},
12:30:25,074 INFO  - 										"xs:element": {
12:30:25,074 INFO  - 											"@name": "DayHigh",
12:30:25,075 INFO  - 											"@type": "xs:decimal",
12:30:25,075 INFO  - 											"@minOccurs": "0"
12:30:25,076 INFO  - 										},
12:30:25,077 INFO  - 										"xs:element": {
12:30:25,077 INFO  - 											"@name": "DayLow",
12:30:25,078 INFO  - 											"@type": "xs:decimal",
12:30:25,078 INFO  - 											"@minOccurs": "0"
12:30:25,078 INFO  - 										},
12:30:25,079 INFO  - 										"xs:element": {
12:30:25,081 INFO  - 											"@name": "StockVolume",
12:30:25,082 INFO  - 											"@type": "xs:int",
12:30:25,083 INFO  - 											"@minOccurs": "0"
12:30:25,083 INFO  - 										},
12:30:25,084 INFO  - 										"xs:element": {
12:30:25,084 INFO  - 											"@name": "PrevCls",
12:30:25,085 INFO  - 											"@type": "xs:decimal",
12:30:25,085 INFO  - 											"@minOccurs": "0"
12:30:25,086 INFO  - 										},
12:30:25,086 INFO  - 										"xs:element": {
12:30:25,087 INFO  - 											"@name": "ChangePercent",
12:30:25,087 INFO  - 											"@type": "xs:string",
12:30:25,088 INFO  - 											"@minOccurs": "0"
12:30:25,088 INFO  - 										},
12:30:25,089 INFO  - 										"xs:element": {
12:30:25,089 INFO  - 											"@name": "FiftyTwoWeekRange",
12:30:25,090 INFO  - 											"@type": "xs:string",
12:30:25,090 INFO  - 											"@minOccurs": "0"
12:30:25,091 INFO  - 										},
12:30:25,091 INFO  - 										"xs:element": {
12:30:25,092 INFO  - 											"@name": "EarnPerShare",
12:30:25,092 INFO  - 											"@type": "xs:decimal",
12:30:25,092 INFO  - 											"@minOccurs": "0"
12:30:25,093 INFO  - 										},
12:30:25,093 INFO  - 										"xs:element": {
12:30:25,094 INFO  - 											"@name": "PE",
12:30:25,094 INFO  - 											"@type": "xs:decimal",
12:30:25,095 INFO  - 											"@minOccurs": "0"
12:30:25,095 INFO  - 										},
12:30:25,096 INFO  - 										"xs:element": {
12:30:25,098 INFO  - 											"@name": "CompanyName",
12:30:25,099 INFO  - 											"@type": "xs:string",
12:30:25,100 INFO  - 											"@minOccurs": "0"
12:30:25,100 INFO  - 										},
12:30:25,101 INFO  - 										"xs:element": {
12:30:25,101 INFO  - 											"@name": "QuoteError",
12:30:25,101 INFO  - 											"@type": "xs:boolean",
12:30:25,102 INFO  - 											"@minOccurs": "0"
12:30:25,102 INFO  - 										},
12:30:25,103 INFO  - 										"xs:element": {
12:30:25,103 INFO  - 											"@name": "AverageDailyVolume",
12:30:25,104 INFO  - 											"@type": "xs:int",
12:30:25,104 INFO  - 											"@minOccurs": "0"
12:30:25,105 INFO  - 										}
12:30:25,105 INFO  - 									}
12:30:25,106 INFO  - 								}
12:30:25,106 INFO  - 							}
12:30:25,107 INFO  - 						}
12:30:25,107 INFO  - 					}
12:30:25,108 INFO  - 				}
12:30:25,108 INFO  - 			},
12:30:25,109 INFO  - 			"diffgr:diffgram": {
12:30:25,109 INFO  - 				"@xmlns:msdata": "urn:schemas-microsoft-com:xml-msdata",
12:30:25,109 INFO  - 				"@xmlns:diffgr": "urn:schemas-microsoft-com:xml-diffgram-v1",
12:30:25,110 INFO  - 				"QuoteData": {
12:30:25,110 INFO  - 					"@xmlns": "",
12:30:25,111 INFO  - 					"Quotes": {
12:30:25,111 INFO  - 						"@diffgr:id": "Quotes1",
12:30:25,112 INFO  - 						"@msdata:rowOrder": "0",
12:30:25,112 INFO  - 						"@diffgr:hasChanges": "inserted",
12:30:25,113 INFO  - 						"StockSymbol": [],
12:30:25,114 INFO  - 						"LastTradeAmount": "0",
12:30:25,115 INFO  - 						"LastTradeDateTime": "0001-01-01T00:00:00+00:00",
12:30:25,115 INFO  - 						"StockChange": "0",
12:30:25,116 INFO  - 						"OpenAmount": "0",
12:30:25,117 INFO  - 						"DayHigh": "0",
12:30:25,117 INFO  - 						"DayLow": "0",
12:30:25,117 INFO  - 						"StockVolume": "0",
12:30:25,118 INFO  - 						"PrevCls": "0",
12:30:25,119 INFO  - 						"EarnPerShare": "0",
12:30:25,119 INFO  - 						"PE": "0",
12:30:25,120 INFO  - 						"QuoteError": "true"
12:30:25,120 INFO  - 					},
12:30:25,121 INFO  - 					"Quotes": {
12:30:25,121 INFO  - 						"@diffgr:id": "Quotes2",
12:30:25,122 INFO  - 						"@msdata:rowOrder": "1",
12:30:25,122 INFO  - 						"@diffgr:hasChanges": "inserted",
12:30:25,122 INFO  - 						"StockSymbol": [],
12:30:25,123 INFO  - 						"LastTradeAmount": "0",
12:30:25,123 INFO  - 						"LastTradeDateTime": "0001-01-01T00:00:00+00:00",
12:30:25,124 INFO  - 						"StockChange": "0",
12:30:25,124 INFO  - 						"OpenAmount": "0",
12:30:25,125 INFO  - 						"DayHigh": "0",
12:30:25,127 INFO  - 						"DayLow": "0",
12:30:25,127 INFO  - 						"StockVolume": "0",
12:30:25,128 INFO  - 						"PrevCls": "0",
12:30:25,128 INFO  - 						"EarnPerShare": "0",
12:30:25,129 INFO  - 						"PE": "0",
12:30:25,129 INFO  - 						"QuoteError": "true"
12:30:25,130 INFO  - 					}
12:30:25,132 INFO  - 				}
12:30:25,132 INFO  - 			}
12:30:25,133 INFO  - 		}
12:30:25,134 INFO  - 	}
12:30:25,135 INFO  - }
```
Ahora, con esta información podemos ejecutar las siguientes líneas de código para ajustar este proceso a un objeto "Entry" de SDI:

```
task.logmsg("########")
var DataArr = [];
var ChildNodes = work["GetQuoteDataSetResponse.GetQuoteDataSetResult.diffgr:diffgram.QuoteData"].getChildNodes();

for(var i = 0; i < ChildNodes.getLength(); i++)
{
	task.logmsg("NODE.item("+i+"): ",ChildNodes.item(i).getAttributes());
	var ChildNodeN = ChildNodes.item(i);
	var NewEntryData = system.newEntry();
	NewEntryData.setAttribute("ID",ChildNodeN["@diffgr:id"]);
	NewEntryData.setAttribute("hasChanges",ChildNodeN["@diffgr:hasChanges"]);
	NewEntryData.setAttribute("rowOrder",ChildNodeN["@msdata:rowOrder"]);		
	//task.logmsg("ScriptConn: ", NewEntryData);
	task.logmsg("ScriptConn: ", NewEntryData);
	DataArr.push(NewEntryData)
}
task.logmsg("ScriptConn: ", DataArr.length);

```
Implementando este código obtenemos la siguiente salida.

```
12:32:49,058 INFO  - ########
12:32:49,059 NODE.item(0):  - [Quotes1, 0, inserted]
12:32:49,061 ScriptConn:  - {
	"hasChanges": "inserted",
	"ID": "Quotes1",
	"rowOrder": "0"
}
12:32:49,062 NODE.item(1):  - [Quotes2, 1, inserted]
12:32:49,063 ScriptConn:  - {
	"hasChanges": "inserted",
	"ID": "Quotes2",
	"rowOrder": "1"
}
12:32:49,064 ScriptConn:  - 2
```
Con esto hemos convertido la respuesta del WebService a objetos que podemos manejar de forma fácil en SDI.

---

Lo primero es presentarles la documentación de este conector de [Script Connector]( https://www.ibm.com/support/knowledgecenter/en/SSCQGF_7.2.0/com.ibm.IBMDI.doc_7.2/referenceguide247.htm#scriptconnect).

Ahora, para el caso de un conector en modo iterador, únicamente nos interesa realizar el script de las siguientes funciones:

**Iterator:**
* selectEntries() - Esta se ejecuta para seleccionar las objetos (Ex: JDBC Conn - SQL Select)
* getNextEntry() - Esta se ejecuta en cada iteración del conector para obtener la siguiente entrada.
* querySchema() - Esta se ejecuta para tener en la UI la información de los atributos.

En esta línea lo primero es el "hook" de la línea general vamos a configurar los parámetros del WebService:


```
var WebServiceWSDL = "http://ws.cdyne.com/delayedstockquote/delayedstockquote.asmx?wsdl";
var WebServiceService = "DelayedStockQuote";
var WebServiceOperation = "GetQuoteDataSet";
var WebServiceEndpoint = "DelayedStockQuoteSoap";
```

Ahora, agregando el conector de script vamos a configurar el conector para obtener los parámetros de forma dinámica, esto lo logramos agregando el siguiente código al "hook" antes de inicializar.

```
thisConnector.setConnectorParam("WebServiceWSDL",WebServiceWSDL);
thisConnector.setConnectorParam("WebServiceService",WebServiceService);
thisConnector.setConnectorParam("WebServiceOperation",WebServiceOperation);
thisConnector.setConnectorParam("WebServiceEndpoint",WebServiceEndpoint);
```

Procedemos a editar función por función, para este vamos a la pestaña de conexión y seleccionamos la opción editar script y reemplazamos la función "querySchema" por el siguiente script.

```
function querySchema ()
{
	var i = new com.ibm.di.entry.Entry();
	i.addAttributeValue("name","ID");
	i.addAttributeValue("syntax","String");
	list.add(i);
	
	var i = new com.ibm.di.entry.Entry();
	i.addAttributeValue("name","hasChanges");
	i.addAttributeValue("syntax","String");
	list.add(i);
	
	var i = new com.ibm.di.entry.Entry();
	i.addAttributeValue("name","rowOrder");
	i.addAttributeValue("syntax","Number");
	list.add(i);
	
	result.setStatus (1);
}
```
Ahora, lo siguiente es crear las variables globales que utilizara el conector.

```
var counter = 0;
var DataArr = [];
```

Estas variables son el contador de entradas y el array con contendrá la información.

El siguiente proceso que vamos a realizar es la función "selectEntries" para esta implementaremos el siguiente código que tiene el siguiente propósito:

* Configuración del conector Axis2 FC
* llamada del WebService
* Normalizar la respuesta a un array de objeto tipo "Entry"
* Retornar el resultado del proceso exitoso

```
function selectEntries()
{
	counter = 0;
	//Confiurar el conector Axis2 FC e inicializarlo para su funcionamiento
	var WSFCConn = system.getFunction("ibmdi.Axis2WSClientFC");
	WSFCConn.setParam("wsdlUrl",connector.getParam("WebServiceWSDL"));
	WSFCConn.setParam("service",connector.getParam("WebServiceService"));
	WSFCConn.setParam("operation",connector.getParam("WebServiceOperation"));
	WSFCConn.setParam("endpoint",connector.getParam("WebServiceEndpoint"));
	WSFCConn.setParam("debug", "true");
	WSFCConn.initialize();
	
	//Creacion de la entrada de la llamada
	var tmpVar = system.newEntry();
	var StockSymbols =  tmpVar.createElementNS("http://ws.cdyne.com/", "StockSymbols")
	var LicenseKey =  tmpVar.createElementNS("http://ws.cdyne.com/", "LicenseKey")
	var GetQuoteDataSet =  tmpVar.createElementNS("http://ws.cdyne.com/", "GetQuoteDataSet")
	StockSymbols.appendChild(tmpVar.createTextNode(","));
	LicenseKey.appendChild(tmpVar.createTextNode("0"));
	GetQuoteDataSet.appendChild(StockSymbols);
	GetQuoteDataSet.appendChild(LicenseKey);
	var ConnEntry = system.newEntry();
	ConnEntry.setAttribute("GetQuoteDataSet",GetQuoteDataSet)
	task.logmsg("ScriptConn: ",ConnEntry);	
	//Llamada del servicio web.
	var response = WSFCConn.perform(ConnEntry);
	//task.logmsg("ScriptConn: ",response);	

	//Creacion del array de respuesta
	var ChildNodes = response["GetQuoteDataSetResponse.GetQuoteDataSetResult.diffgr:diffgram.QuoteData"].getChildNodes();
	
	for(var i = 0; i < ChildNodes.getLength(); i++)
	{
		task.logmsg("NODE.item("+i+"): ",ChildNodes.item(i).getAttributes());
		var ChildNodeN = ChildNodes.item(i);
		var NewEntryData = system.newEntry();
		NewEntryData.setAttribute("ID",ChildNodeN["@diffgr:id"]);
		NewEntryData.setAttribute("hasChanges",ChildNodeN["@diffgr:hasChanges"]);
		NewEntryData.setAttribute("rowOrder",ChildNodeN["@msdata:rowOrder"]);		
		//task.logmsg("ScriptConn: ", NewEntryData);
		DataArr.push(NewEntryData)
	}
	//task.logmsg("ScriptConn: ", DataArr.length);
	
	//Finalizacion exitosa.
	WSFCConn.terminate();
	result.setStatus(1);
	result.setMessage ("New Entry");
}
```
Con esto solo nos queda la función "getNextEntry" que nos retornara uno a uno las entradas del array

```
function getNextEntry ()
{
	if (counter >= DataArr.length) {
		result.setStatus (0);
		result.setMessage ("End of input");
		return;
	}
	var CurrEntry = DataArr[counter];
	entry.merge(CurrEntry);
	counter++;
	result.setStatus(1);
	result.setMessage("Script OK");
}
```
Ahora, agregamos un script para imprimir una a una las entradas.

```
// Dump the work entry
task.logmsg("########")
task.dumpEntry(work);
```

Ahora, al ejecutar la línea con estas configuración obtenemos la siguiente salida.

```

12:53:32,906 INFO  - CTGDIS115I Starting scripting hook of type prolog0.
12:53:32,914 INFO  - CTGDIS116I Scripting hook of type prolog0 finished.
12:53:33,485 ScriptConn:  - {
	"GetQuoteDataSet": {
		"StockSymbols": ",",
		"LicenseKey": "0"
	}
}
12:53:33,727 NODE.item(0):  - [Quotes1, 0, inserted]
12:53:33,728 NODE.item(1):  - [Quotes2, 1, inserted]
12:53:33,729 INFO  - CTGDIS087I Iterating.
12:53:33,731 INFO  - ########
12:53:33,733 INFO  - CTGDIS003I *** Start dumping Entry
12:53:33,733 INFO  - 	Operation: generic
12:53:33,734 INFO  - 	Entry attributes:
12:53:33,735 INFO  - 		ID (replace):	'Quotes1'
12:53:33,736 INFO  - 		hasChanges (replace):	'inserted'
12:53:33,736 INFO  - 		rowOrder (replace):	'0'
12:53:33,737 INFO  - CTGDIS004I *** Finished dumping Entry
12:53:33,741 INFO  - ########
12:53:33,741 INFO  - CTGDIS003I *** Start dumping Entry
12:53:33,742 INFO  - 	Operation: generic
12:53:33,743 INFO  - 	Entry attributes:
12:53:33,745 INFO  - 		ID (replace):	'Quotes2'
12:53:33,747 INFO  - 		hasChanges (replace):	'inserted'
12:53:33,748 INFO  - 		rowOrder (replace):	'1'
12:53:33,750 INFO  - CTGDIS004I *** Finished dumping Entry
12:53:33,751 INFO  - CTGDIS088I Finished iterating.
12:53:33,752 INFO  - CTGDIS100I Printing the Connector statistics.
12:53:33,753 INFO  -  [CustomConnector] Get:2
12:53:33,755 INFO  -  [DumpWorkEntry] Calls: 2
12:53:33,756 INFO  - CTGDIS104I Total: Get:2.
12:53:33,757 INFO  - CTGDIS101I Finished printing the Connector statistics.
12:53:33,757 INFO  - CTGDIS080I Terminated successfully (0 errors).

```

[SDI-SciptConnector](https://gitlab.com/snippets/1894427)
