---
title: Autenticación SSL de SDI como cliente
subtitle: Autenticación SSL de SDI como cliente
date: 2019-07-06
comments: true
tags: ["SDI", "SSL", "TDI", "Security Directory Integrator", "Client Auth"]
---

Durante algunas actividades realizadas me encontré con el requerimiento de realizar una conexión de SDI con un servicio web que requería autenticación por medio de certificados digitales, por suerte SDI incluye esta funcionalidad por defecto, pero para validar que SDI esta presentando el certificado correcto decidí realizar el siguiente proceso.
Primero vamos a usar el siguiente recurso web [cryptomix]( https://server.cryptomix.com/secure/)  que nos permitirá validar esta información.
![cryptomix Web](/post/img/SDI-Client-Auth/cryptomix.PNG "cryptomix Web")

Ahora, lo primero que tenemos que tener en cuenta es la línea necesaria para realiza nuestras pruebas, para esto crearemos una línea básica con un conector HTTP en modalidad cliente con un parser XML, después incluiremos un script para imprimir toda la información de la respuesta.

![SSL AL](/post/img/SDI-Client-Auth/SSLAl.PNG "SSL AL")

Para el caso del script para imprimir la información aplicaremos el siguiente código:

```
// Dump the work entry
task.logmsg(work["http.body"].response);
```

Con las configuraciones iniciales podemos realizar la primera prueba de ejecución obteniendo los siguientes resultados:

![AL Run 1](/post/img/SDI-Client-Auth/ALRun1.PNG "AL Run 1")

Observamos el proceso exitoso que se esta ejecutando, ahora para personalizar el proceso necesitamos realizar la creación de un certificado personal y la configuración necesaria del archivo solutions.properties

Primero debemos encontrar el archivo solutions.properties ubicado en la ruta <SDI-Solution-Path> (EJ. <SDI-Inst-Path>/tdisol)

Y se debe buscar las siguientes propiedades. (Si se va a usar el mismo llavero por defecto, mi recomendación es siempre realizar una copia de seguridad para poder restaurarlo sin incidentes)

```
## client authentication
javax.net.ssl.keyStore=testkey.jks
{protect}-javax.net.ssl.keyStorePassword={encr}a3p9Ou/g41YSm/RCS1Qp1KmKh7Q+38YUO+3QoTmbyzOMl/rSDJHZ7Ty2Hv108s2wk71SJmfqwKpDchNIwFTR+vAB9U4FfGRTxil/puS372ncQJvEU7avzrN45CPZd8M4a4lBKOtrFEyy+p0Qoq1bWVrvrdOLQs+n5EeBIDYPmAo=
javax.net.ssl.keyStoreType=jks
```

Estas propiedades deben ser ajustadas a las necesidades que tengamos, para este ejemplo no realizaremos modificaciones.

Para generar el certificado utilizaremos la línea de comandos y la utilidad java de seguridad, esta esta bicada en al siguiente ruta.

```
Cd <sdi-inst-path>/jvm/jre/bin
Ls -lrt | grep key

root@DESKTOP-GAPE3OR:/mnt/d/Programs/IBM/SDI/V7.2/jvm/jre/bin# ls -lrt | grep key
-rwxrwxrwx 1 gabriel gabriel   17584 May  2  2018 keytool.exe
-rwxrwxrwx 1 gabriel gabriel   17584 May  2  2018 ikeyman.exe
-rwxrwxrwx 1 gabriel gabriel   17584 May  2  2018 ikeycmd.exe
root@DESKTOP-GAPE3OR:/mnt/d/Programs/IBM/SDI/V7.2/jvm/jre/bin#
```

El siguiente es el comando que se debe ejecutar para realizar la creación de un certificado auto firmado.

```
./keytool -genkeypair -keystore <TDIkeyfile> -storepass <tdiKeyFilePass> -alias ClientAuthTest -keyalg RSA -sigalg SHA1withRSA -keysize 2048 -dname ‘<name>’ -v
```

![Cert Creation](/post/img/SDI-Client-Auth/CertCreation.PNG "Cert Creation")

Algo para tener en cuenta es que en este llavero únicamente debe existir un certificado personal, de lo contrario el Servidor de SDI no iniciara de forma correcta.
Una vez finalizada la creación del certificado y la configuración del archivo solutions.properties se debe reiniciar el servidor de SDI para que las nuevas configuraciones tengan efecto.

Al ejecutar nuevamente la AL posterior al reinicio observamos que se muestra de forma correcta el certificado.

![AL Run 2](/post/img/SDI-Client-Auth/ALRun2.PNG "AL Run 2")

