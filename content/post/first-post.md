---
title: About me
subtitle: Why you'd want to hang out with me
date: 2019-02-21
comments: false
tags: ["Bienvenidos", "About Me"]
---


Ingeniero electrónico y especialista en diseño de videojuegos, me dedico a la gestión de identidades y el control de acceso con las siguientes certificaciones:

- IBM Certified Deployment Professional - Identity Governance and Intelligence V5.2
- IBM Certified Deployment Professional - Security Access Manager for Web V7.0
- IBM Certified Deployment Professional - Security Identity Manager V6.0



