---
title: Uso de SDI para el consumo de recursos REST
subtitle: Uso de SDI para el consumo de recursos REST
date: 2019-07-06
comments: true
tags: ["SDI", "REST", "TDI", "Security Directory Integrator"]
---

Últimamente he buscado la forma de documentar los trabajos o las actividades que realizo de SDI de alguna forma, y encontré en este blog una forma rápida para hacerlo, entonces decidí realizar una línea sencilla para explicar el uso de SDI consultando servicios REST.

Para esto decidí usar recursos gratuitos que se encuentran en internet en este caso usaremos [jsonplaceholder](https://jsonplaceholder.typicode.com/) que nos provee múltiples servicios REST para realizar pruebas, puntualmente usaremos los servicios [/users]( https://jsonplaceholder.typicode.com/users) y [/todos]( https://jsonplaceholder.typicode.com/todos)

Lo primero será crear una línea básica de TDI, esta debe tener la siguiente estructura:

![SDI AL](/post/img/SDI-REST/UsersTODOS.PNG "SDI AL")

Iniciamos con el conector “GetUsers”, este conector se encuentra en modo iterador y debe leer todos los registros del servicio [/users]( https://jsonplaceholder.typicode.com/users), el límite configurado para la lectura debe ser “0” o vacio y leeremos los siguientes atributos (el parser configurado para este conector es JSON): 

![GetUsers Connector](/post/img/SDI-REST/GetUsersParams.PNG " GetUsers Connector")

En la pestaña de conexión ingresaremos la URL del servicio [/users]( https://jsonplaceholder.typicode.com/users)

![GetUsers Connection](/post/img/SDI-REST/GetUsersCon.PNG " GetUsers Connection")

Una vez configurado y cargado el certificado en el llavero correspondiente podremos realizar la conexión de prueba para obtener los atributos.

![GetUsers Attributes](/post/img/SDI-REST/GetUsersAttr.PNG " GetUsers Attributes")

El siguiente conector es “ForEach_UserTODO”, este conector es un conector FOR-EACH y debe heredar las características de un conector HTTP en modo iterador, nuevamente el límite configurado para la lectura debe ser “0” o vacio y leeremos los siguientes atributos (el parser configurado para este conector es JSON): 

![ForEach_UserTODO Connector](/post/img/SDI-REST/EFConn.PNG " ForEach_UserTODO Connector")

Para la conexion a este conector vamos a tener que utilizar una URL dinámica, esta dependerá del campo “userId” leído del primer llamado REST de la siguiente forma:

```
https://<url-servicio-REST>/todos?userid=<codifo-id-usuario>
```

Con este formato presente generamos el siguiente código:

```
/*
**Incluimos el replace ya que realizando algunas pruebas encontramos que SDI entrega el ID del usuario con formato “n.0”
**Se incluye el getvalue() ya que al obtener el atributo userId ya que SDI incluye el nombre del campo al obtener el atributo y únicamente necesitamos el valor.
*/
ret.value = "https://jsonplaceholder.typicode.com/todos?userId=" + work.userId.getValue().replace(".0","");
```

![ForEach_UserTODO Connection](/post/img/SDI-REST/EFCon.PNG " ForEach_UserTODO Connection")

![ForEach_UserTODO URL Script](/post/img/SDI-REST/EFURLScript.PNG " ForEach_UserTODO URL Script")

Y realizaremos la conexión con el servicio para validar los atributos a leer:

![ForEach_UserTODO Attr](/post/img/SDI-REST/EFAttr.PNG " ForEach_UserTODO Attr")

El ultimo conector que vamos a utilizar es un conector básico de archivo, este debe escribir un archivo de texto plano CSV (el parser configurado para este conector es CSV con separador ;)
Y escribirá todos los atributos leídos de los dos servicios REST
![FCRead Conn](/post/img/SDI-REST/FCRead.PNG "FCRead Conn")

En este conector observamos los siguientes scripts:

Para el caso del atributo address, necesitamos la información de la ciudad al validar la estructura de este atributo encontramos que es un mapa de attributos, SDI nos permite acceder a estos atributos como miembros del objeto “address” de la siguiente forma:

```
//Attr address
work.address.city
```

Para el caso de los atributos id y userID nos encontramos con el mismo caso evidenciado en el durante el proceso de generar la URL dinámica para el el llamado del servicio [/todos]( https://jsonplaceholder.typicode.com/todos)

```
//Attr id
work.id.getValue().replace(".0","")
```

```
//Attr userId
work.userId.getValue().replace(".0","")
```

Una vez configuramos todos los conectores necesarios procedemos a realizar la ejecución de la línea de SDI.

![AL Run Log](/post/img/SDI-REST/ALRunLog.PNG "AL Run Log")

Al validar el archivo de salida encontramos que la información se escribe de forma correcta.

![AL Out File](/post/img/SDI-REST/ALOutFile.PNG "AL Out File")

Con esto comprobamos que podemos realizar el consumo de servicios REST por medio de SDI de forma rápida.

El siguiente es el archivo de configuración de SDI que se creó para este ejemplo:

[SDI-REST](https://gitlab.com/snippets/1872609)

