---
title: Unreal Engine 5 Enhanced Input C++
subtitle: Plugin "Enhanced Input C++"
date: 2021-06-18
comments: false
tags: ["UE5", "Unreal Engine 5", "Enhanced Input", "C++"]
---

Unreal Engine 5 provee una nueva forma de gestionar los comandos de entradas de los jugadores, para esto es posible habilitar un nuevo plugin con la funcionalidad.

![Habilitar Enhanced Input Plugin](/post/img/UE-EnhancedInput/Habilita.png "Habilitar Enhanced Input Plugin")

Posterior a esto es necesario realizar un reinicio del editor.

El siguiente paso es realizar es realizar la asignación de la nueva clase para el "PlayerInputComponent":

![Asignar Enhanced Input Class](/post/img/UE-EnhancedInput/InputClass.png "Asignar Enhanced Input Class")

Para finalizar las configuraciones necesarias es necesario agregar la dependencia a el archivo de compilación.

```
PublicDependencyModuleNames.AddRange(new string[] { "EnhancedInput" });		

```

De esta forma el archivo "\<Proyecto>.Build.cs" quedara de la siguiente forma:

```
// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class EnhancedInputTut : ModuleRules
{
	public EnhancedInputTut(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
	
		PublicIncludePaths.AddRange(new string[] { "EnhancedInputTut" });
		
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore" });

		PublicDependencyModuleNames.AddRange(new string[] { "EnhancedInput" });	
		
		PrivateDependencyModuleNames.AddRange(new string[] {  });

		// Uncomment if you are using Slate UI
		// PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });
		
		// Uncomment if you are using online features
		// PrivateDependencyModuleNames.Add("OnlineSubsystem");

		// To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true
	}
}

```



Procedemos a realizar la creacion de un nuevo actor para el caso realizaremos la creacion de un actor tipo "Character" y l

```cpp
//EICharacter.h
// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "EICharacter.generated.h"

UCLASS()
class ENHANCEDINPUTTUT_API AEICharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEICharacter();

protected:
	
	UPROPERTY(VisibleAnywhere, Category="Components")
	TObjectPtr<class USpringArmComponent> SpringArm;

	UPROPERTY(VisibleAnywhere, Category="Components")
	TObjectPtr<class UCameraComponent> Camera;
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
```
```cpp
//EICharacter.cpp
// Fill out your copyright notice in the Description page of Project Settings.
#include "Characters/EICharacter.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"

// Sets default values
AEICharacter::AEICharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;\
	SpringArm = CreateDefaultSubobject<USpringArmComponent>("SpringArm");
	if (!SpringArm)
	{
		return;
	}
	SpringArm->SetupAttachment(GetRootComponent(), NAME_None);
	Camera = CreateDefaultSubobject<UCameraComponent>("Camera");
	if (!Camera)
	{
		return;
	}
	Camera->SetupAttachment(SpringArm, NAME_None);
}

// Called when the game starts or when spawned
void AEICharacter::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AEICharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AEICharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}


```

Para implementar el funcionamiento es necesario definir las acciones que se van a implementar en el personaje con las siguientes declaraciones de "UInputAction" y "UInputMappingContext":

```cpp
UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
TObjectPtr<class UInputAction> MovementAction;

UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
TObjectPtr<class UInputAction> JumpAction;

UPROPERTY(EditDefaultsOnly, Category = "Components")
TObjectPtr<class UInputMappingContext> MappingContext;
```
Ahora debemos implementar las acciones que serán utilizadas por nuestras acciones para aplicar los valores de las entradas:

```cpp
UFUNCTION()
void ScrollMovementAction(const struct FInputActionValue& Value);
UFUNCTION()
void BeginJumpAction();
UFUNCTION()
void EndJumpAction();
```

```cpp
void AEICharacter::ScrollMovementAction(const FInputActionValue& Value)
{
	AddMovementInput(GetActorForwardVector(),Value[0]);
}

void AEICharacter::BeginJumpAction()
{
	Jump();
}

void AEICharacter::EndJumpAction()
{
	StopJumping();
}
```


Ahora debemos agregar asignar las acciones implementando la función "SetupPlayerInputComponent" en el actor:

```cpp
void AEICharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	if (UEnhancedInputComponent* PlayerEnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerInputComponent))
	{
		if (MovementAction)
		{
			PlayerEnhancedInputComponent->BindAction(MovementAction, ETriggerEvent::Triggered, this, &AEICharacter::ScrollMovementAction);
		}
		if (JumpAction)
		{
			PlayerEnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Started, this, &AEICharacter::BeginJumpAction);
			PlayerEnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Completed, this, &AEICharacter::EndJumpAction);
		}
	}	
}
```

Posterior a la implementación de la función "SetupPlayerInputComponent", es necesario implementar la función "PawnClientRestart", esta función será la encargada de manejar las asignaciones de las entradas hacia el actor:

```cpp
virtual void PawnClientRestart() override;
```

```cpp
void AEICharacter::PawnClientRestart()
{
	Super::PawnClientRestart();
	if (APlayerController* PC = Cast<APlayerController>(GetController()))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PC->GetLocalPlayer()))
		{
			Subsystem->ClearAllMappings();
			Subsystem->AddMappingContext(MappingContext, 0);
		}
	}
}
```

Con esto estará lista la mayor parte del código y podremos comenzar a crear los blueprints necesarios, para esto crearemos inicialmente un blueprint hijo de nuestra clase "AEICharacter", lo ubicaremos en el mundo y nos aseguraremos de que el actor siempre sea poseído por el jugador0

![BP Creados](/post/img/UE-EnhancedInput/bps1.png "BP Creados")

![Mundo](/post/img/UE-EnhancedInput/worldloc.png "Mundo")

Posterior a eso nos aseguraremos de rotar nuestro personaje con orientación al eje X y la cámara con vista lateral al personaje, adicionalmente únicamente permitiremos el movimiento del personaje en el eje X.

![Configuracion personaje](/post/img/UE-EnhancedInput/bpcharacter.png "Configuracion personaje")

Ahora comenzaremos a crear los nuevos blueprints necesarios para las nuevas clases del "Enhanced Input

![Enhanced Input](/post/img/UE-EnhancedInput/eimc.png "EnhancedInput")

Para el caso crearemos dos blueprints de tipo "Input Action", estos serian "IA_MovementAction" y "IA_JumpAction", para el caso de "Input Mapping Context" crearemos el blueprint "IM_Context"

![BP Creados](/post/img/UE-EnhancedInput/bps2.png "BP Creados")

En el caso del blueprint "IA_MovementAction" no asignaremos triggers y asignaremos una entrada de tipo Axis1D.

![IA_MovementAction](/post/img/UE-EnhancedInput/MovementAction.png "IA_MovementAction")

En el caso del blueprint "IA_JumpAction" asignaremos las acciones que deben disparar el evento de jump, para esto asignaremos los triggers "Pressed" y "Released" y una entrada de tipo bool.

![IA_JumpAction](/post/img/UE-EnhancedInput/JumpAction.png "IA_JumpAction")

En el blueprint de context asignaremos los imput map de la forma en la que se se necesite, para el caso asignaremos una entrada de axis y una entrada boolen

![IM_Context](/post/img/UE-EnhancedInput/Context.png "IM_Context")

Posterior a esto asignaremos estos blueprints en el blueprint del personaje que creamos con anterioridad.

![Configuracion input personaje](/post/img/UE-EnhancedInput/bpcharacter2.png "Configuracion input personaje")

Ahora podemos probar el funcionamiento del personaje

![Resultado](/post/img/UE-EnhancedInput/Animation.gif "Resultado")


El proyecto completo puede ser descargado [aqui](https://gitlab.com/p30virus/enhancedinput)

